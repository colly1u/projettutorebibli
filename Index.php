<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 22/06/2015
 * Time: 08:28
 */
require_once 'vendor/autoload.php';
use src\singleton\ConnectionFactory;
use src\bibliapp\control\AdminController;
use Slim\Slim;

$connect=ConnectionFactory::getConnection('src/conf/db.bibliapp.conf.ini');
$app = new Slim(['templates.path'=>'src/web/templates']);
$admin= new AdminController();
session_start();



$app->get('/',function() use( $admin){
    $admin->home();

});

$app->get('/home',function() use( $admin){
    $admin->home();

});

$app->get('/formInscription',function() use( $admin){
    $admin->formulaireInscription();

});

$app->get('/formLogin',function() use( $admin){
    $admin->formulaireLogin();

});

$app->post('/verifFormLogin',function() use( $admin){
    $admin->verifFormulaireLogin();

});

$app->post('/verifFormInscription',function() use( $admin){
    $admin->verifFormulaireInscription();

});

$app->get('/deconnexion',function() use( $admin){
    $admin->deconnexion();

});

$app->get('/formISBN',function() use( $admin){
    $admin->formulaireISBN();


});

$app->post('/verifFormISBN',function() use( $admin){
    $admin->verifFormulaireISBN();

});

$app->post('/ajouterLivre',function() use ($admin){
    $admin->ajouterLivre();

});

$app->get('/bibliotheque',function() use( $admin){
    $admin->maBibliotheque();

});

$app->get('/isbn/:id', function($id) use($admin) {
    $admin->afficherLivreBibliotheque($id);

});


$app->post('/supprimerBibli/:id', function($id) use($admin) {
    $admin->supprimerLivreBibliotheque($id);


});


$app->get('/profil',function() use( $admin){
    $admin->home();

});

$app->get('/emprunt',function() use( $admin){
    $admin->formEmprunt();

});

$app->get('/bibliEmprunt',function() use( $admin){
    $admin->biliothequeEmprunt();

});

$app->post('/empruntByTitre',function() use ($admin){
    $admin->listeLivreByTitre();


});

$app->get('/emprunter/:isbn',function($isbn) use( $admin){
    $admin->listeEmprunt($isbn);

});


$app->post('/emprunter/:isbn/:id',function($isbn, $id) use( $admin){
    $admin->finalEmprunt($isbn,$id);

});


$app->post('/rendre/:isbn/:id',function($isbn, $id) use( $admin){
    $admin->rendreEmprunt($isbn,$id);

});








$app->run();

?>



