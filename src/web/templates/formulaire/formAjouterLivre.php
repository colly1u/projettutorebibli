
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <form class="form-horizontal"role="form" method="post" action="ajouterLivre">
                    <fieldset>
                        <legend>Ajouter le livre a votre bibliotheque</legend>



                        <div class="form-group">
                            <div class="col-lg-6 col-lg-offset-4">
                                <button type="reset" class="btn btn-default"
                                        onclick="self.location.href='home'">Annuler</button>
                                <button type="submit" name="valider" value="ok" class="btn btn-primary">Ajouter</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

</section>


