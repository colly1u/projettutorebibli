<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <form class="form-horizontal"role="form" method="post" action="verifFormISBN">
                    <fieldset>
                        <legend>Entrez code ISBN a 10 ou 13 chiffres</legend>

                        <div class="form-group">
                            <label for="inputISBN" class="col-lg-2 control-label">ISBN</label>
                            <div class="col-lg-10">
                                <input type="number" class="form-control" name="ISBN"
                                       placeholder="ex: 9782070396849">
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-lg-6 col-lg-offset-4">
                                <button type="reset" class="btn btn-default"
                                        onclick="self.location.href='home'">Annuler</button>
                                <button type="submit" name="valider" value="ok" class="btn btn-primary">Valider</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

</section>