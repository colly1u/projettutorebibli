
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <form class="form-horizontal"role="form" method="post" action="verifFormLogin">
                    <fieldset>
                        <legend>Connexion</legend>

                        <div class="form-group">
                            <label for="inputPseudo" class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="inputEmail"
                                       placeholder="Email">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                            <div class="col-lg-10">
                                <input type="password" class="form-control" name="inputPassword"
                                       placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-6 col-lg-offset-4">
                                <button type="reset" class="btn btn-default"
                                        onclick="self.location.href='home'">Annuler</button>
                                <button type="submit" name="valider" value="ok" class="btn btn-primary">Valider</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

</section>