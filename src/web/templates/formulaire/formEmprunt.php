<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <form class="form-horizontal" role="form" method="post" action="empruntByTitre">
                    <fieldset>
                        <legend>Rechercher livre par son titre</legend>

                        <div class="form-group">
                            <label for="titre" class="col-lg-2 control-label">titre</label>
                            <div class="col-lg-10">
                                <input required="required" type="text" class="form-control" name="titre"
                                       placeholder="Ttitre">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-6 col-lg-offset-4">
                                <button type="reset" class="btn btn-default"
                                        onclick="self.location.href='home'">Annuler</button>
                                <button type="submit" name="valider" value="ok" class="btn btn-primary">Valider</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

</section>