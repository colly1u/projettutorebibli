
<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3">
				<form class="form-horizontal" role="form" method="post" action="verifFormInscription">
					<fieldset>
						<legend>Inscription</legend>

						<div class="form-group">
							<label for="inputEmail" class="col-lg-2 control-label">Email</label>
							<div class="col-lg-10">
								<input required="required" type="email" class="form-control" name="inputEmail"
									placeholder="Email">
							</div>
						</div>

						

						<div class="form-group">
							<label for="inputPassword" class="col-lg-2 control-label">Password</label>
							<div class="col-lg-10">
								<input required="required" type="password" class="form-control" name="inputPassword"
									placeholder="Password">
							</div>
						</div>




						<div class="form-group">
							<label for="inputNom" class="col-lg-2 control-label">Nom</label>
							<div class="col-lg-10">
								<input required="required" type="text" class="form-control" name="inputNom"
									placeholder="Nom">
							</div>
						</div>


						<div class="form-group">
							<label for="inputPrenom" class="col-lg-2 control-label">Prenom</label>
							<div class="col-lg-10">
								<input required="required" type="text" class="form-control" name="inputPrenom"
									placeholder="Prenom">
							</div>
						</div>


						<div class="form-group">
							<label for="inputAdresse" class="col-lg-2 control-label">Adresse</label>
							<div class="col-lg-10">
								<input required="required" type="text" class="form-control"name="inputAdresse"
									placeholder="Adresse">
							</div>
						</div>
							
						<div class="form-group">
							<label for="inputAge" class="col-lg-2 control-label">Age</label>
							<div class="col-lg-10">
								<input required="required" type="number" class="form-control" name="inputAge"
									placeholder="Age">
							</div>
						</div>
						

						<div class="form-group">
							<label for="inputDate" class="col-lg-2 control-label">date de
								naissance</label>
							<div class="col-lg-2 col-lg-offset-1">
                                <input required="required" type="number" class="form-control" name="inputDateD"
                                       placeholder="ex: 6">
							</div>
							<div class="col-lg-4">
                                <select class="form-control" name="inputDateM">
                                    <option value="01">Janvier</option>
                                    <option value="02">Fevrier</option>
                                    <option value="03">Mars</option>
                                    <option value="04">Avril</option>
                                    <option value="05">Mai</option>
                                    <option value="06">Juin</option>
                                    <option value="07">Juillet</option>
                                    <option value="08">Aout</option>
                                    <option value="09">Septembre</option>
                                    <option value="10">Octobre</option>
                                    <option value="11">Novembre</option>
                                    <option value="12">Decembre</option>
                                </select>
							</div>
							<div class="col-lg-3">
								<input required="required" type="number" class="form-control" name="inputDateY"
									placeholder="ex:1994">
							</div>
						</div>



						<div class="form-group">
							<label for="inputTel" class="col-lg-4 control-label">Numero
								de telephone </label>
							<div class="col-lg-8">
								<input required="required" type="text" class="form-control" name="inputTel"
									placeholder="06(ou 07).xx.xx.xx.xx">
							</div>
						</div>







						<div class="form-group">
							<div class="col-lg-6 col-lg-offset-4">
								<button type="reset" class="btn btn-default"
									onclick="self.location.href='home'">Annuler</button>
								<button type="submit" name="valider" value="ok" class="btn btn-primary">Valider</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>

</section>