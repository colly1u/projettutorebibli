<html>
<head>
    <title>Bibli</title>
    <?php
    $app = \Slim\Slim::getInstance();
    $css = $app->request()->getRootUri().'/src/web/css/BootStrap.css';
    $formInscr = $app->request()->getRootUri().'/formInscription';
    $img = $app->request()->getRootUri().'/src/web/img/home_icon.png';
    $log = $app->request()->getRootUri().'/formLogin';
    $home = $app->request()->getRootUri().'/';
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $css?>"/>
</head>
<body>
<header>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="btn btn-link" href="<?php echo $home?>"><img src="<?php echo $img?>"></a>
                <a class="btn btn-link" href="<?php echo $formInscr?>">Inscription</a>
                <a class="btn btn-link" href="<?php echo $log?>">Connexion</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">About</a></li>
                </ul>
            </div>
    </nav>


</header>