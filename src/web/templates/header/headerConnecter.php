<html>
<head>
    <title>Bibli</title>

    <?php
    $app = \Slim\Slim::getInstance();
    $css = $app->request()->getRootUri().'/src/web/css/BootStrap.css';
    $img = $app->request()->getRootUri().'/src/web/img/home_icon.png';
    $deco= $app->request()->getRootUri().'/deconnexion';
    $home = $app->request()->getRootUri().'/';
    $profil=$app->request()->getRootUri().'/profil';
    $isbn=$app->request()->getRootUri().'/formISBN';
    $bibli=$app->request()->getRootUri().'/bibliotheque';
    $emprunt=$app->request()->getRootUri().'/emprunt';
    $bibliEmprunt=$app->request()->getRootUri().'/bibliEmprunt';


    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $css?>"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
</head>
<body>
<header>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="btn btn-link" href="<?php echo $home?>"><img src="<?php echo $img ?>"></a>
                <a class="btn btn-link" href="<?php echo $profil?>">Profil</a>
                <a class="btn btn-link" href="<?php echo $isbn?>">Rechercher un livre</a>
                <a class="btn btn-link" href="<?php echo $emprunt?>">Emprunter un livre</a>
                <a class="btn btn-link" href="<?php echo $bibli?>">Ma bibliotheque</a>
                <a class="btn btn-link" href="<?php echo $bibliEmprunt?>">Mes emprunts</a>



            </div>




            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo $deco ?>">Deconnexion</a></li>
                </ul>
            </div>
        </div>


    </nav>

    <center><h1>Bibli app</h1></center>
</header>