<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 22/06/2015
 * Time: 11:01
 */
namespace src\bibliapp\vue;
use Slim\Slim;

class VuePrincipale  {

    private $app;

    function  __construct(){
        if(!isset($app)){
            $this->app = Slim::getInstance();
        }

    }


    public function ajouterISBN(){
        $this->app->render('formulaire/formAjouterLivre.php');
    }



    public function header(){
        $this->app->render('header/header.php');

    }

    public function headerConnecte(){
        $this->app->render('header/headerConnecter.php');

    }


    public function formLogin(){
        $this->app->render('formulaire/formLogin.php');

    }

    public function  formInscription(){
        $this->app->render('formulaire/formInscription.php');

    }

    public function formISBN(){
        $this->app->render('formulaire/formISBN.php');

    }
    public function formEmprunt(){
        $this->app->render('formulaire/formEmprunt.php');

    }


}