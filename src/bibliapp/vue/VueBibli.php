<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 22/06/2015
 * Time: 08:58
 */
namespace src\bibliapp\vue;
use Slim\Slim;
use src\bibliapp\control\BibliController;

class VueBibli {

    private $app;

    function  __construct(){
        if(!isset($app)){
            $this->app = Slim::getInstance();
        }

    }


    function afficherUnLivre ($livre){
        $html='<section>
	        <div class="container">
		        <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <center><p><h3>ISBN :'.$livre['isbn'].'</h3></p></center><br>
                        <p> auteur :'.$livre['auteur'].'<br>
                        langue:'.$livre['langue'].' <br>
                        date de publication: '.$livre['publication'].'<br></p>';

        if($livre['images']!=null){
            $html=$html.'<p> couverture: <img src="'. $livre['images'].'" alt="Mon livre" >';
        }
        $html=$html. '</center>
			</div>
		</div>
	    </div>
        </section>';

        return $html;

    }


    function afficherBibliotheque($tablivre){
        $html='<table class="table table-striped table-hover ">
         <thead>
        <tr>
            <th>ISBN</th>
            <th>Titre</th>
            <th>Auteur</th>
            <th>Emprunte</th>
        </tr>
        </thead>
        <tbody>';

        foreach($tablivre as $ligne){
            foreach($ligne as $attri=>$val) {

                if ($attri == "isbn") {
                    $html = $html .'<tr><td><a href="isbn/'. $val .'">'.$val.'</td>';

                    $control=new BibliController();
                    $livre=$control->findLivreByISBN($val);
					foreach($livre as $objet){
                        $html=$html.'<td>'.$objet['titre'].'</td><td>'.$objet['auteur'].'</td>';
					}
                }

                if($attri=="emprunt"){
                    if($val==0){
                        $html=$html.'<td> non </td></tr>';


                    }else{
                        $user=$control->findEmpruntByEmprunteurIdAndIsbn($ligne['isbn'],$_SESSION['utilisateur_id']);
                        $proprio=$control->findUtilisateurById($user[0]['proprietaire']);
                        $html=$html.'<td> oui <br> par '.$proprio[0]['email'].'</td></tr>';

                    }
                }
            }



        }
        $html+$html+' </tbody></table>';
        return($html);
    }



    function afficherUnLivreBibliotheque ($livre){
        $app = \Slim\Slim::getInstance();
        $lien = $app->request()->getRootUri().'/supprimerBibli/'.$livre[0]['isbn'];
        $html='<form class="form-horizontal"role="form" method="post" action="'. $lien. '">

        <section>
	        <div class="container">
		        <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <center><p><h3>ISBN :'.$livre[0]['isbn'].'</h3></p></center><br>
                        <p> auteur :'.$livre[0]['auteur'].'<br>
                        langue:'.$livre[0]['langue'].' <br>
                        date de publication: '.$livre[0]['publication'].'<br></p>';

        if($livre[0]['images']!=null){
            $html=$html.'<p> couverture: <img src="'. $livre[0]['images'].'" alt="Mon livre" >';
        }
        $html=$html. '</center>
			</div>

			<div class="form-group">
                            <div class="col-lg-6 col-lg-offset-4">
                                <button type="reset" class="btn btn-default"
                                        onclick="self.location.href="home"">Annuler</button>
                                <button type="submit" name="valider" value="ok" class="btn btn-primary">Supprimer ce livre de ma bibliothèque</button>
                            </div>
            </div>


		</div>
	    </div>
        </section>
        </form>';

        return $html;

    }

    function afficherBibliothequeEmprunt($tabEmprunt){
        $control=new BibliController();
        $html='<table class="table table-striped table-hover ">
         <thead>
        <tr>
            <th>proprietaire</th>
            <th>isbn</th>
            <th>titre</th>
            <th></th>

        </tr>
        </thead>
        <tbody>';

        foreach($tabEmprunt as $ligne){
            foreach($ligne as $attri=>$val) {

                if ($attri == "livre_emprunte") {
                    $html = $html .'<td><a href="isbn/'. $val .'">'.$val.'</td>';
                    $livre=$control->findLivreByISBN($val);
                    $html = $html .'<td>'.$livre[0]['titre'].'</td>';
                    $html=$html.'<td><form class="form-horizontal" role="form" method="post" action="rendre/'.$val.'/'.$_SESSION['utilisateur_id'].'">'.
                     '<button type="submit" name="valider" value="ok" class="btn btn-primary">Rendre le livre</button>
                        </form></td></tr>';


                }

                if($attri=="proprietaire"){

                    $user=$control->findUtilisateurById($val);
                    $html=$html.'<tr><td>'.$user[0] ['email'].' </td>';

                }
            }



        }
        $html+$html+' </tbody></table>';
        return($html);
    }


    function afficherLivrePourEmprunt($tabLivre){
        $html='<table class="table table-striped table-hover ">
         <thead>
        <tr>
            <th>ISBN</th>
            <th>Titre</th>
            <th>Auteur</th>
        </tr>
        </thead>
        <tbody>';
        foreach($tabLivre as $ligne) {
            foreach ($ligne as $attri => $val) {
                if ($attri == "isbn") {
                    $html = $html .'<tr><td><a href="emprunter/'. $val .'">'.$val.'</td>';

                    $control=new BibliController();
                    $livre=$control->findLivreByISBN($val);
                    foreach($livre as $objet){
                        $html=$html.'<td>'.$objet['titre'].'</td><td>'.$objet['auteur'].'</td>';
                    }
                }

            }
        }
        $html+$html+' </tbody></table>';
        return($html);

    }


    function afficherEmprunt($tabEmprunt){
        $html='<table class="table table-striped table-hover ">
         <thead>
        <tr>
            <th>titre</th>
            <th>image</th>
            <th>proprietaire</th>
            <th>emprunté</th>
            <th></th>
        </tr>
        </thead>
        <tbody>';
        $control=new BibliController();

        foreach($tabEmprunt as $ligne){
            foreach($ligne as $attri=>$val) {

                if ($attri == "isbn") {
                    $livre=$control->findLivreByISBN($val);
                    $html = $html .'<tr><td>'.$livre[0]['titre'].'</td>';
                    $html = $html .'<td>'.'<img src="'. $livre[0]['images'].'" alt="Mon livre" >'.'</td>';


                }

                if($attri=="utilisateur_id"){

                    $user=$control->findUtilisateurById($val);
                    $html=$html.'<td>'.$user[0]['email'].' </td>';


                }
                if($attri=="emprunt") {
                    if ($attri == 0) {
                        $html = $html . '<td> non  </td><td><form class="form-horizontal" role="form" method="post" action="'.$ligne['isbn'].'/'.$ligne['utilisateur_id'].'">
                     <button type="submit" name="valider" value="ok" class="btn btn-primary">Emprunter</button>
                        </form></td></tr>';


                    } else {
                        $html = $html . '<td> oui </td> <td></td> </tr>';

                    }
                }
            }



        }
        $html+' </tbody></table>';
        return($html);
    }






}