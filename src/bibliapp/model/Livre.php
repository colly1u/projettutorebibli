<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 16/09/2015
 * Time: 08:55
 */
namespace src\bibliapp\model;
use Illuminate\Database\Eloquent\Model;

class Livre extends Model {

    protected $table = 'livre';
    protected $primaryKey = 'isbn';
    public $timestamps = false;
    public $incrementing = false;




}