<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 22/06/2015
 * Time: 08:57
 */
namespace src\bibliapp\model;
use Illuminate\Database\Eloquent\Model;

class Client extends Model {

    protected $table = 'client';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function utilisateur()
    {
        return $this->hasOne('Utilisateur');
    }


}