<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 16/09/2015
 * Time: 08:55
 */
namespace src\bibliapp\model;
use Illuminate\Database\Eloquent\Model;

class Bibliotheque extends Model {

    protected $table = 'bibliotheque';
    protected $primaryKey = null;
    public $timestamps = false;
    public $incrementing = false;




}