<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 22/06/2015
 * Time: 08:57
 */
namespace src\bibliapp\model;
use Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model
{

    protected $table = 'utilisateur';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function client()
    {
        return $this->belongsTo('Client');
    }
}