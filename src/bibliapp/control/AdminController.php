<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 22/06/2015
 * Time: 09:38
 */
namespace src\bibliapp\control;
use src\bibliapp\control\InsertionController;
use src\bibliapp\vue\VueBibli;
use src\bibliapp\vue\VuePrincipale;
use src\bibliapp\control\BibliController;
use Slim\Slim;

class AdminController {

    private $vue,$vueBibli,$controlInsert,$controlBibli;

    public function __construct(){
        $this->vue=new VuePrincipale();
        $this->controlInsert=new InsertionController();
        $this->controlBibli= new BibliController();
        $this->vueBibli= new VueBibli();
    }

    public function home(){
        if(isset($_SESSION['email'])){
            if( $_SESSION['ip'] != $_SERVER['REMOTE_ADDR']){
                $this->vue->header();
                echo ' vous avez changer d ip';
                session_destroy();
            }else{
                $this->vue->headerConnecte();

            }
        }else{
            $this->vue->header();
        }
    }

    public function formulaireInscription(){

        $this->vue->header();
        $this->vue->formInscription();


    }

    public function formulaireISBN(){

        $this->home();
        $this->vue->formISBN();



    }

    public function verifFormulaireISBN(){
        $this->home();
        $livre=$this->controlBibli->verifierISBN();
        if(count($livre)==0){
            echo("Le livre demande est introuvable");

        }else {
            echo($this->vueBibli->afficherUnLivre($livre));
            echo($this->vue->ajouterISBN());

        }

    }

    public function ajouterLivre(){
        $this->controlBibli->ajouterLivre();
        $this->home();
        echo(" le livre a ete ajouter a votre bibliotheques");

    }


    public function verifFormulaireInscription(){
        $this->vue->header();
        $this->controlInsert->ajouterClient();


    }

     public function formulaireLogin(){
         $this->vue->header();
         $this->vue->formLogin();

     }

    public function deconnexion(){
        session_destroy();
        $this->vue->header();

    }

    public function verifFormulaireLogin(){
        $res=$this->controlBibli->verifierConnexion();
        if (!$res){
            $this->vue->header();

        }else{
            $this->vue->headerConnecte();

        }

    }

    public function maBibliotheque(){
        $this->home();
        $bibli=$this->controlBibli->findLivreByBibliotheque(($_SESSION['utilisateur_id']));
        $res=$this->vueBibli->afficherBibliotheque($bibli);
        echo($res);


    }


    public function afficherLivreBibliotheque($isbn){
        $this->home();
        $livre=$this->controlBibli->findLivreByISBN($isbn);
        echo($this->vueBibli->afficherUnLivreBibliotheque($livre));


    }


    public function supprimerLivreBibliotheque($isbn){
        $this->controlInsert->supprimerLivreBibliotheque($isbn);
        $this->home();
        echo("le Livre a bien été supprimé");

    }

    public function biliothequeEmprunt(){
        $this->home();
        $emprunt=$this->controlBibli->findEmpruntByUtilisateur($_SESSION['utilisateur_id']);
        $res=$this->vueBibli->afficherBibliothequeEmprunt($emprunt);
        echo($res);

    }


    public function formEmprunt(){
        $this->home();
        $this->vue->formEmprunt();

    }

    public function listeLivreByTitre(){
        $this->home();
        $post=Slim::getInstance()->request->post();
        $liste=$this->controlBibli->findLivreByTitre($post['titre']);
        echo($this->vueBibli->afficherLivrePourEmprunt($liste));

    }


    public function listeEmprunt($isbn){
        $this->home();
        $livre=$this->controlBibli->findLivreBibliothequeByIsbn($isbn);
        echo($this->vueBibli->afficherEmprunt($livre));

    }

    public function finalEmprunt($isbn,$id){
        $this->home();
        $this->controlInsert->ajouterEmprunt($id,$isbn);

    }

    public function rendreEmprunt($isbn,$id){
        $this->home();
        $this->controlInsert->supprimerEmprunt($isbn,$id);
        echo('livre supprimé');
    }

}