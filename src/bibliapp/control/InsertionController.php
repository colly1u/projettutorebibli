<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 22/06/2015
 * Time: 08:55
 */
namespace src\bibliapp\control;
use src\bibliapp\control\BibliController;
use src\bibliapp\model\Bibliotheque;
use src\bibliapp\model\Client;
use src\bibliapp\model\Emprunt;
use src\bibliapp\model\Utilisateur;
use src\bibliapp\model\Livre;
use Slim\Slim;


class InsertionController {

    public function ajouterClient(){

        $post=Slim::getInstance()->request->post();
        if((!isset($post['valider'])|| ($post['valider']!='ok'))){
            echo('Formulaire vide <br>');


        }elseif (!filter_var($post['inputAge'],FILTER_VALIDATE_INT)){
            echo('l age rentre est incorecte <br>');

        }elseif (!filter_var($post['inputEmail'],FILTER_VALIDATE_EMAIL )) {

            echo('l email rentre est incorecte <br>');

        }elseif(!filter_var($post['inputDateD'],FILTER_VALIDATE_INT)||!filter_var($post['inputDateY'],FILTER_VALIDATE_INT)){
            echo('la date de naissance rentre est incorecte');

        }elseif($post['inputDateD']<1 || $post['inputDateD']>31){
            echo('Votre jour de naissance n existe pas <br>');

        }elseif($post['inputDateY']>date('YYYY',time()) || $post['inputAge']<0){
            echo '<br>'.date('YYYY');
            echo('annee de Naissance invalide ou/et age inferieur a 0 <br>');

        }else{
            $control=new BibliController();
            $res=$control->findUtilisateurByEmail(filter_var($post['inputEmail'],FILTER_SANITIZE_STRING));
            if(count($res==0)) {
                $client = new Client();
                $client->nom = filter_var($post['inputNom'], FILTER_SANITIZE_STRING);
                $client->prenom = filter_var($post['inputPrenom'], FILTER_SANITIZE_STRING);
                $client->adresse = filter_var($post['inputAdresse'], FILTER_SANITIZE_STRING);
                $date = '';
                if ($post['inputDateD'] > 9) {
                    $date = $post['inputDateD'] . '-';
                } else {
                    $date = '0' . $post['inputDateD'] . '-';
                }
                $date = $date . $post['inputDateM'] . '-'. $post['inputDateY'];
                $date=filter_var($date, FILTER_SANITIZE_STRING);
                $client->dateNais =  date('Y-m-d',strtotime($date));
                $client->age = filter_var($post['inputAge'], FILTER_SANITIZE_NUMBER_INT);
                $client->numTel = filter_var($post['inputTel'], FILTER_SANITIZE_STRING);
                $client->save();

                $user=new Utilisateur();
                $user->client_id=$client->id;
                $user->email=filter_var($post['inputEmail'],FILTER_SANITIZE_EMAIL);
                $alea=array('cost'=> 12);
                $hash=password_hash($post['inputPassword'], PASSWORD_DEFAULT ,$alea);
                $user->pass=$hash;
                $user->alea=$alea['cost'];
                $user->date_enregistrement = date('Y-m-d',time());
                $user->IP_enregistrement=$_SERVER['REMOTE_ADDR'];

                $user->client_id=$client->id;
                $user->save();
                echo('inscription valide');
            }
        }
    }


    public function ajouterLivre(){
        $livre=new Livre();
        $livre->isbn=$_SESSION['livre']['isbn'];
        $livre->titre=$_SESSION['livre']['titre'];
        $livre->auteur=$_SESSION['livre']['auteur'];
        $livre->langue=$_SESSION['livre']['langue'];
        $livre->publication=$_SESSION['livre']['publication'];

        $livre->pages=$_SESSION['livre']['pages'];


        $livre->images=$_SESSION['livre']['images'];
        $livre->save();
    }


    public function ajouterLivreBibliotheque(){
        $bibli=new Bibliotheque();

        $bibli->isbn=$_SESSION['isbnActuel'];
        $bibli->utilisateur_id=$_SESSION['utilisateur_id'];
        $bibli->emprunt=0;
        $bibli->save();
        unset($_SESSION['isbnActuel']);


    }


    public function supprimerLivreBibliotheque($isbn){
        $livre=Bibliotheque::where('isbn', '=', $isbn)
            ->where('utilisateur_id', '=',  $_SESSION['utilisateur_id'])
            ->delete();

    }

    public function ajouterEmprunt($id,$isbn){
        $emprunt=new Emprunt();
        $emprunt->livre_emprunte=$isbn;
        $emprunt->proprietaire=$id;
        $emprunt->emprunteur=$_SESSION['utilisateur_id'];
        $emprunt->save();
        $bibli=Bibliotheque::where('isbn', '=', $isbn)
            ->where('utilisateur_id', '=',  $id)->update(['emprunt'=>1]);
    }


    public function supprimerEmprunt($isbn,$id){
        $livre=Emprunt::where('livre_emprunte', '=', $isbn)
            ->where('emprunteur', '=',  $_SESSION['utilisateur_id'])
            ->delete();
        $bibli=Bibliotheque::where('isbn', '=', $isbn)
            ->where('utilisateur_id', '=',  $_SESSION['utilisateur_id'])->update(['emprunt'=>0]);


    }


}