<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 22/06/2015
 * Time: 09:06
 */

namespace src\bibliapp\control;
use src\bibliapp\model\Bibliotheque;
use src\bibliapp\model\Emprunt;
use src\bibliapp\model\Utilisateur;
use src\bibliapp\model\Livre;
use Slim\Slim;
use src\bibliapp\control\InsertionController;
use src\bibliapp\vue\VueBibli;

class BibliController {

    //Utilisateur

    public function findUtilisateurByEmail($email){
        $user=Utilisateur::where('email', '=', $email)->get();
        return($user->toArray());
    }

    public function findUtilisateurById($id ){
        $user=Utilisateur::where('id', '=', $id)->get();
        return($user->toArray());
    }


    public function verifierConnexion(){
        $res=false;
        $post=Slim::getInstance()->request->post();
        if((!isset($post['valider'])|| ($post['valider']!='ok'))){

        }else{
            $user=$this->findUtilisateurByEmail($post['inputEmail']);
            if(count($user)==1 && password_verify($post['inputPassword'],$user[0]['pass'])) {

                $_SESSION['email'] = $post['inputEmail'];
                $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
                $_SESSION['client_id'] = $user[0]['client_id'];
                $_SESSION['utilisateur_id'] = $user[0]['id'];

                $res=true;

            }
        }
        return $res;
    }


    //Livre
    public function findLivreByISBN($isbn){
        $livre=Livre::where('isbn', '=', $isbn)->get();
        return($livre->toArray());
    }

    public function verifierISBN(){
        unset($_SESSION['isbnActuel']);

        unset($_SESSION['livre']);
        $post=Slim::getInstance()->request->post();
        if(isset($post['valider'])){
            $livre=$this->findLivreByISBN($post['ISBN']);

            if(count($livre)==1){
                $livre=$livre[0];
                $_SESSION['isbnActuel']=$post['ISBN'];


            }else{
                $request = 'https://www.googleapis.com/books/v1/volumes?q=isbn:' . $post['ISBN'];
                $response = file_get_contents($request);
                $results = json_decode($response);

                if($results->totalItems > 0) {
                    // avec de la chance, ce sera le 1er trouvé
                    $book = $results->items[0];
                    $livre=new Livre();
                    $livre->isbn = $post['ISBN'];
                    $livre->titre = $book->volumeInfo->title;
                    $livre->auteur = $book->volumeInfo->authors[0];
                    $livre->langue = $book->volumeInfo->language;
                    $livre->publication = $book->volumeInfo->publishedDate;
                    $livre->pages = $book->volumeInfo->pageCount;

                    if (isset($book->volumeInfo->imageLinks)) {
                        $livre->images = str_replace('&edge=curl', '', $book->volumeInfo->imageLinks->thumbnail);
                    }

                    $livre->toArray();
                    $_SESSION['livre']=$livre;

                    $controlInsert= new InsertionController();
                    $controlInsert->ajouterLivre();
                    $_SESSION['isbnActuel']=$post['ISBN'];
                    unset($_SESSION['livre']);


                }

            }
        }


        return $livre;


    }



    public function ajouterLivre(){
        $post=Slim::getInstance()->request->post();
        if(isset($post['valider'])){
            $controlInsert= new InsertionController();
            $controlInsert->ajouterLivreBibliotheque();
        }


    }



    public function findLivreByBibliotheque($id){
        $livre=Bibliotheque::where('utilisateur_id', '=', $id)->get();
        return($livre->toArray());

    }

    public function findLivreBibliothequeByIsbn($isbn){
        $livre=Bibliotheque::where('isbn', '=', $isbn)->get();
        return($livre->toArray());

    }

    public function findEmpruntByUtilisateur($id){
        $emprunt=Emprunt::where('emprunteur','=',$id)->get();
        return($emprunt->toArray());
    }


    public function findLivreByTitre($titre){
        $livre=Livre::where('titre','LIKE','%'.$titre.'%')->get();
        return($livre->toArray());
    }

    public function findEmpruntByEmprunteurIdAndIsbn($isbn,$id){
        $emprunt=Emprunt::where('emprunteur','=',$id)->
            where('livre_emprunte','=',$isbn)
            ->get();
        return($emprunt->toArray());
    }



}