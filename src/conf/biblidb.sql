-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 02 Novembre 2015 à 02:08
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `biblidb`
--

-- --------------------------------------------------------

--
-- Structure de la table `bibliotheque`
--

CREATE TABLE IF NOT EXISTS `bibliotheque` (
  `isbn` bigint(15) NOT NULL,
  `utilisateur_id` int(10) NOT NULL,
  `emprunt` tinyint(1) NOT NULL,
  KEY `isbn` (`isbn`,`utilisateur_id`),
  KEY `utilisateur_id` (`utilisateur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `bibliotheque`
--

INSERT INTO `bibliotheque` (`isbn`, `utilisateur_id`, `emprunt`) VALUES
(2070396843, 7, 0);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `prenom` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `adresse` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `numTel` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `dateNais` date NOT NULL,
  `age` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`id`, `nom`, `prenom`, `adresse`, `numTel`, `dateNais`, `age`) VALUES
(1, 'Florian', 'Colly', '6 rue des bl', '0648512438', '0000-00-00', 14),
(2, 'Florian', 'Colly', '6 rue des bl', '0648512438', '0000-00-00', 14),
(3, 'qdsf', 'qdqd', 'rfgd', '0648512438', '0000-00-00', 1),
(4, 'qdsf', 'qdqd', 'rfgd', '0648512438', '1994-04-06', 1),
(5, 'Colly', 'Florian', '49 rue de Lawou', '0648512438', '1996-03-04', 19),
(6, 'Colly', 'Florian', '49 rue de Lawou', '0648512438', '1994-01-08', 414),
(7, 'Colly', 'Florian', '49 rue de Lawou', '0648512438', '1994-01-08', 14),
(8, 'admin', 'admin', '6 4dsfq', '070644546', '1994-01-05', 18);

-- --------------------------------------------------------

--
-- Structure de la table `emprunt`
--

CREATE TABLE IF NOT EXISTS `emprunt` (
  `proprietaire` int(10) NOT NULL,
  `emprunteur` int(10) NOT NULL,
  `livre_emprunte` bigint(15) NOT NULL,
  KEY `proprietaire` (`proprietaire`,`emprunteur`,`livre_emprunte`),
  KEY `emprunteur` (`emprunteur`),
  KEY `livre_emprunte` (`livre_emprunte`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `livre`
--

CREATE TABLE IF NOT EXISTS `livre` (
  `isbn` bigint(15) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `auteur` varchar(100) NOT NULL,
  `langue` varchar(30) NOT NULL,
  `publication` varchar(30) NOT NULL,
  `pages` int(5) NOT NULL,
  `images` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `livre`
--

INSERT INTO `livre` (`isbn`, `titre`, `auteur`, `langue`, `publication`, `pages`, `images`) VALUES
(0, 'ISBN user''s manual', 'National Library of Canada', 'en', '1984', 52, NULL),
(553593714, 'A Game of Thrones', 'George R. R. Martin', 'en', '2011-03-01', 864, 'http://books.google.fr/books/content?id=CgvUQgAACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api'),
(2070396843, 'Chagrin d''école', 'Daniel Pennac', 'fr', '2009', 297, 'http://books.google.fr/books/content?id=FN9qPwAACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api'),
(2070769178, 'Chagrin d''école', 'Daniel Pennac', 'fr', '2007', 304, 'http://books.google.fr/books/content?id=ojtlAAAAMAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api'),
(2840504960, 'Baudelaire', 'André Guyaux', 'fr', '2007', 1143, 'http://books.google.fr/books/content?id=jPaoQU-Jnm4C&printsec=frontcover&img=1&zoom=1&source=gbs_api'),
(9782070124855, 'Journal d''un corps', 'Daniel Pennac', 'fr', '2012', 389, NULL),
(9782070612888, 'Le seigneur des anneaux', 'J. R. R. Tolkien', 'fr', '2007-08-01', 717, 'http://books.google.fr/books/content?id=7gJhHwAACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api'),
(9782072410741, 'Chagrin d''école', 'Daniel Pennac', 'fr', '2012-10-16T00:00:00+02:00', 306, 'http://books.google.fr/books/content?id=iTtgCKenzA4C&printsec=frontcover&img=1&zoom=1&source=gbs_api'),
(9782840118336, 'Chagrin d''école', 'Daniel Pennac', 'fr', '2008-05-01', 380, NULL),
(9782840504962, 'Baudelaire', 'André Guyaux', 'fr', '2007', 1143, 'http://books.google.fr/books/content?id=jPaoQU-Jnm4C&printsec=frontcover&img=1&zoom=1&source=gbs_api');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date_enregistrement` date NOT NULL,
  `ip_enregistrement` varchar(100) NOT NULL,
  `alea` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `client_id`, `email`, `pass`, `date_enregistrement`, `ip_enregistrement`, `alea`) VALUES
(1, 2, 'mapo@hotmail.fr', '$2y$12$mtq.ML87WwXz.gooEBHXQ.YMcJWT.tFZJAA55AHMKgL2iOkydehXy', '2015-06-22', '::1', '12'),
(2, 3, 'dqfsd@dfsg.fr', '$2y$12$tXhy3bDvhKd3wh/51Z9U3OBRR9NQGAeJBPLTqlZHss1/4RnSXzbLO', '2015-06-22', '::1', '12'),
(3, 4, 'dsf@gfsg.fd', '$2y$12$CORiA2kRxF9debwg55r7I.tWDvyaduZQcO8WP9TeuUTvFkHwqb9ua', '2015-06-22', '::1', '12'),
(4, 5, 'colly@iut.fr', '$2y$12$mFW4JebxFr/f/23P/kt4nOTt7z56Ke4Xhj1B.mDG2sbms/UlcD7xO', '2015-06-22', '::1', '12'),
(5, 6, 'scxv@fog.fr', '$2y$12$yWkdhQFTC5usrycvEYxaUO5i9KYu5cXtwcw/5fBwPRSTxn751vb5a', '2015-06-23', '::1', '12'),
(6, 7, 'admin@bibli.fr', '$2y$12$kYiTHAFyWx5OL7NDAxIxBOj6kA0whWP6li3TF5pPOTIb4s3ho8udy', '2015-06-23', '::1', '12'),
(7, 8, 'admin@hotmail.fr', '$2y$12$1UUCEZ3/LxPdJOX/TR4Su.MVhrNWpPSz.00UmeI19axinXBfyFrTG', '2015-09-02', '::1', '12');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `bibliotheque`
--
ALTER TABLE `bibliotheque`
  ADD CONSTRAINT `bibliotheque_ibfk_1` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateur` (`id`),
  ADD CONSTRAINT `bibliotheque_ibfk_2` FOREIGN KEY (`isbn`) REFERENCES `livre` (`isbn`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur/client` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
