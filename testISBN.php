<header>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </header>



<?php
/**
 * Created by PhpStorm.
 * User: Florian
 * Date: 23/06/2015
 * Time: 08:49
 */
$isbn=9782070396849;
$request = 'https://www.googleapis.com/books/v1/volumes?q=isbn:' . $isbn;
$response = file_get_contents($request);
$results = json_decode($response);

if($results->totalItems > 0) {
    // avec de la chance, ce sera le 1er trouvé
    $book = $results->items[0];

    $infos['isbn'] = $book->volumeInfo->industryIdentifiers[0]->identifier;
    $infos['titre'] = $book->volumeInfo->title;
    $infos['auteur'] = $book->volumeInfo->authors[0];
    $infos['langue'] = $book->volumeInfo->language;
    $infos['publication'] = $book->volumeInfo->publishedDate;
    $infos['pages'] = $book->volumeInfo->pageCount;

    if (isset($book->volumeInfo->imageLinks)) {
        $infos['image'] = str_replace('&edge=curl', '', $book->volumeInfo->imageLinks->thumbnail);
    }
    print_r($infos);
    var_dump($infos);
    echo('<img src="'. $infos['image'].'" alt="Mon livre" >');

}

?>